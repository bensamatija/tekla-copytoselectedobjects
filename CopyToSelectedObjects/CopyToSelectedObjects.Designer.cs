﻿namespace CopyToSelectedObjects
{
    partial class CopyToSelectedObjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyToSelectedObjects));
            this.Btn_CopyToSelectedObjects = new System.Windows.Forms.Button();
            this.Chk_DeleteSourceObjects = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Btn_CopyToSelectedObjects
            // 
            this.Btn_CopyToSelectedObjects.Font = new System.Drawing.Font("Consolas", 8.125F);
            this.Btn_CopyToSelectedObjects.Location = new System.Drawing.Point(76, 15);
            this.Btn_CopyToSelectedObjects.Margin = new System.Windows.Forms.Padding(6);
            this.Btn_CopyToSelectedObjects.Name = "Btn_CopyToSelectedObjects";
            this.Btn_CopyToSelectedObjects.Size = new System.Drawing.Size(300, 100);
            this.Btn_CopyToSelectedObjects.TabIndex = 1;
            this.Btn_CopyToSelectedObjects.Text = "Copy To / Replace Selected Objects";
            this.Btn_CopyToSelectedObjects.UseVisualStyleBackColor = true;
            this.Btn_CopyToSelectedObjects.Click += new System.EventHandler(this.Btn_CopyToSelectedObjects_Click);
            // 
            // Chk_DeleteSourceObjects
            // 
            this.Chk_DeleteSourceObjects.AutoSize = true;
            this.Chk_DeleteSourceObjects.Font = new System.Drawing.Font("Consolas", 8.125F);
            this.Chk_DeleteSourceObjects.Location = new System.Drawing.Point(76, 127);
            this.Chk_DeleteSourceObjects.Margin = new System.Windows.Forms.Padding(6);
            this.Chk_DeleteSourceObjects.Name = "Chk_DeleteSourceObjects";
            this.Chk_DeleteSourceObjects.Size = new System.Drawing.Size(296, 30);
            this.Chk_DeleteSourceObjects.TabIndex = 2;
            this.Chk_DeleteSourceObjects.Text = "Delete Father Objects";
            this.Chk_DeleteSourceObjects.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // CopyToSelectedObjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(832, 200);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Chk_DeleteSourceObjects);
            this.Controls.Add(this.Btn_CopyToSelectedObjects);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(10, 10);
            this.Name = "CopyToSelectedObjects";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Btn_CopyToSelectedObjects;
        private System.Windows.Forms.CheckBox Chk_DeleteSourceObjects;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

