﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.Runtime.Remoting;
using Tekla.Structures.Internal;
using System.Diagnostics;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

namespace CopyToSelectedObjects
{
    public partial class CopyToSelectedObjects : Form
    {
        public CopyToSelectedObjects()
        {
            InitializeComponent();
            CustomSetup();
        }

        string appName = "CopyTo/ReplaceSelectedObjects";

        // Get the current model:
        TSM.Model model = new TSM.Model();
        //T3D.CoordinateSystem copyBaseCS = new T3D.CoordinateSystem();


        //private void Btn_CreateBeam_Click(object sender, EventArgs e)
        //{
        //    TSM.Model myModel = new TSM.Model();
        //    TSM.Beam myBeam = new TSM.Beam(new T3D.Point(1000, 1000, 1000), new T3D.Point(6000, 6000, 6000));
        //    myBeam.Material.MaterialString = "S235JR";
        //    myBeam.Profile.ProfileString = "HEA400";
        //    myBeam.DeformingData.Angle = myBeam.DeformingData.Angle2 = 45;
        //    myBeam.Insert();
        //    myModel.CommitChanges();
        //}

        private void CustomSetup()
        {
            this.AutoSize = false;
            this.MinimumSize = new Size(0, 0);
            //this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            //this.ClientSize = new System.Drawing.Size(170, 90);
            this.ClientSize = new System.Drawing.Size(200, 90);
            this.Text = " v.1.1";
        }

        private void Btn_CopyToSelectedObjects_Click(object sender, EventArgs e)
        {
            CopySelectedObjectsToSelectedObjects();
        }

        /// <summary>
        /// Copy Selected Objects to Selected Objects
        /// </summary>
        private void CopySelectedObjectsToSelectedObjects()
        {
            try
            {
                // Create a backup of current Transformation Plane:
                //TSM.TransformationPlane transformationPlaneBackup = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();

                //### Select <Source> Object:
                TSM.Object sourceObj = SelectPart("Pick father part");


                //### Select <Copy> Object(s):
                TSM.ModelObjectEnumerator moeCopy = new TSM.UI.Picker().PickObjects(TSMUI.Picker.PickObjectsEnum.PICK_N_OBJECTS, "Pick child part(s) for Copying. Continue with Middle Mouse Button");
                List<TSM.Object> objCopyList = AddModelSelectionToList(moeCopy);


                //### Select <CopyTo> Object(s):
                TSM.ModelObjectEnumerator moeDist = new TSM.UI.Picker().PickObjects(TSMUI.Picker.PickObjectsEnum.PICK_N_OBJECTS, "Pick new father part(s) to Copy to. Finish with Middle Mouse Button");
                TSO.Operation.DisplayPrompt(appName + " - Calculating ...");
                List<TSM.Object> objDestList = AddModelSelectionToList(moeDist);

                // Also add parts to list that weren't selected by picking:                 // Doesn't work for now
                //objDestList = AlternativeSelection(objDestList);


                // Timing the execution function:
                Stopwatch stopwatch = Stopwatch.StartNew();


                // Iterate through list of destination objects and Instantiate a list of objects with appropriate properties:
                IterateThroughDestinationObjectsAndInstantiate(sourceObj, objDestList, objCopyList);


                // Restore Workplane:
                //model.GetWorkPlaneHandler().SetCurrentTransformationPlane(transformationPlaneBackup);


                model.CommitChanges();
                stopwatch.Stop();
                TSO.Operation.DisplayPrompt("Copy To Selected Objects - Done in " + stopwatch.ElapsedMilliseconds + " ms");
            }
            catch (Exception ex) { print(ex.Message); TSO.Operation.DisplayPrompt(ex.Message.ToString()); }
        }

        /// <summary>
        /// Add Model Selection to the List
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        private List<TSM.Object> AddModelSelectionToList(TSM.ModelObjectEnumerator objects)
        {
            List<TSM.Object> objList = new List<TSM.Object>();
            try
            {
                foreach (TSM.Object o in objects)
                {
                    //print(o.ToString());

                    if (o.ToString() == "Tekla.Structures.Model.Beam")
                    {
                        TSM.Part p = o as TSM.Part;
                        objList.Add(o);
                    }
                    if (o.ToString() == "Tekla.Structures.Model.ContourPlate")
                    {
                        TSM.ContourPlate p = o as TSM.ContourPlate;
                        objList.Add(o);
                    }
                    else if (o.ToString() == "Tekla.Structures.Model.PolyBeam")
                    {
                        TSM.PolyBeam p = o as TSM.PolyBeam;
                        objList.Add(o);
                    }
                    else if (o.ToString() == "Tekla.Structures.Model.BoltArray")
                    {
                        TSM.BoltGroup p = o as TSM.BoltGroup;
                        objList.Add(o);
                    }
                }
            }
            catch (Exception ex) { print(ex.Message); TSO.Operation.DisplayPrompt(ex.Message.ToString()); }

            return objList;
        }

        /// <summary>
        /// Copy Selected Objects from "objCopyList" to "objDestList". Use "sourceObj" to get coordinate system.
        /// </summary>
        /// <param name="sourceObj"></param>
        /// <param name="objDestList"></param>
        /// <param name="objCopyList"></param>
        private void IterateThroughDestinationObjectsAndInstantiate(TSM.Object sourceObj, List<TSM.Object> objDestList, List<TSM.Object> objCopyList)
        {
            try
            {
                // Get CooSys of the Source object:
                TSM.Part sourceObj_asPart = sourceObj as TSM.Part;
                T3D.CoordinateSystem sourceObjCS = sourceObj_asPart.GetCoordinateSystem();

                // Create the list for the objects to be removed:
                var objRemovalList = new List<TSM.Object>();

                // For every object we are copying to:
                foreach (TSM.Object d in objDestList)
                {
                    TSM.Part p = d as TSM.Part;
                    T3D.CoordinateSystem destObjCS = p.GetCoordinateSystem();

                    // For every object we are copying:
                    foreach (TSM.Object o in objCopyList)
                    {
                        // This is the object we are copying to now:
                        //TSM.Part x = o as TSM.Part;

                        if (o.ToString() == "Tekla.Structures.Model.Beam")
                        {
                            TSM.Beam pa = o as TSM.Beam;
                            TSO.Operation.CopyObject(pa, sourceObjCS, destObjCS);
                        }
                        else if (o.ToString() == "Tekla.Structures.Model.Part")
                        {
                            TSM.Part pa = o as TSM.Part;
                            TSO.Operation.CopyObject(pa, sourceObjCS, destObjCS);
                        }
                        else if (o.ToString() == "Tekla.Structures.Model.PolyBeam")
                        {
                            TSM.PolyBeam pa = o as TSM.PolyBeam;
                            TSO.Operation.CopyObject(pa, sourceObjCS, destObjCS);
                        }
                        else if (o.ToString() == "Tekla.Structures.Model.ContourPlate")
                        {
                            TSM.ContourPlate pa = o as TSM.ContourPlate;
                            TSO.Operation.CopyObject(pa, sourceObjCS, destObjCS);
                        }
                        else if (o.ToString() == "Tekla.Structures.Model.BoltArray")
                        {
                            TSM.BoltGroup pa = o as TSM.BoltGroup;
                            TSO.Operation.CopyObject(pa, sourceObjCS, destObjCS);
                        }
                    }
                    // Add father objects to the list for removing:
                    if (Chk_DeleteSourceObjects.Checked == true)
                    {
                        objRemovalList.Add(p);
                    }
                }
                // Delete all the father objects from list:
                if (Chk_DeleteSourceObjects.Checked == true)
                {
                    foreach (TSM.Part o in objRemovalList)
                    {
                        o.Delete();
                    }
                }
            }
            catch (Exception ex) { print(ex.Message); TSO.Operation.DisplayPrompt(ex.Message.ToString()); }
        }

        /// <summary>
        /// Select object by picking it in the model view
        /// </summary>
        /// <param name="taskPrompt"></param>
        /// <returns></returns>
        private TSM.Object SelectPart(string taskPrompt)
        {
            TSM.Object myObject = null;
            try
            {
                TSMUI.Picker picker = new TSMUI.Picker();
                myObject = picker.PickObject(TSMUI.Picker.PickObjectEnum.PICK_ONE_PART, taskPrompt) as TSM.Object;
            }
            catch (Exception ex) { print(ex.Message); TSO.Operation.DisplayPrompt(ex.Message.ToString()); }

            return myObject;
        }

        ///// <summary>
        ///// Alternative selection. Ex.: select parts with drawings.
        ///// </summary>
        ///// <param name="objDestList"></param>
        ///// <returns></returns>
        //private List<TSM.Object> AlternativeSelection(List<TSM.Object> objDestList)
        //{
        //    TSM.ModelObjectEnumerator Objects = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.BEAM);
        //    foreach (TSM.Part obj in Objects)
        //    {
        //        if (obj != null)
        //        {
        //            objDestList.Add(obj);
        //        }
        //    }

        //    Objects = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.CONTOURPLATE);
        //    foreach (TSM.Part obj in Objects)
        //    {
        //        if (obj != null)
        //        {
        //            objDestList.Add(obj);
        //        }
        //    }

        //    Objects = model.GetModelObjectSelector().GetAllObjectsWithType(TSM.ModelObject.ModelObjectEnum.POLYBEAM);
        //    foreach (TSM.Part obj in Objects)
        //    {
        //        if (obj != null)
        //        {
        //            objDestList.Add(obj);
        //        }
        //    }

        //    return objDestList;
        //}

        public double RadToDeg(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        public void print(string p)
        {
            Console.WriteLine(p);
        }
    }
}
